// Should return a function that invokes `cb`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `cb` again.
// `cb` should only ever be invoked once for a given set of arguments.
function cacheFunction(cb) {
    let cacheObj = {};
    return function invokeCb(arg) {
        if (cacheObj.arg) {
            return cacheObj.arg;
        } else {
            cacheObj.arg = cb();
            return cacheObj.arg;
        }
    }
}
function cb() {
    // let num = Math.floor(Math.random() * 100);
    // return num;
    console.log(`invoked`)
}
let test = cacheFunction(cb)
let ans1 = test("first")
console.log(ans1)
let ans2 = test("first")
console.log(ans2)

module.exports = cacheFunction;
