// * Return an object that has two methods called `increment` and `decrement`.
// * `increment` should increment a counter variable in closure scope and return it.
// * `decrement` should decrement the counter variable and return it.
function counterFactory() {
    return {
        increment: function (n) {
            return n += 1
        },
        decrement: function (n) {
            return n -= 1
        }
    }
}
let n = 10
// console.log(counterFactory().decrement(n))
// console.log(counterFactory().increment(n))

module.exports = counterFactory()