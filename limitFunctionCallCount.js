// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned
function limitFunctionCallCount(cb, n) {
    return function invokeCB() {
        cb(n)
    }
}
// function cb(n) {
//     while (n > 0) {
//         n -= 1
//         console.log(`cb invoked`)
//     }
//     console.log(`limit exceeded to invoke this function `)
//     return n
// }
// n = 5
// let ans = limitFunctionCallCount(cb, n)
// console.log(ans)

module.exports = limitFunctionCallCount