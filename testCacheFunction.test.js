const cacheFunc = require("./cacheFunction")

function cb() {
    let num = Math.floor(Math.random() * 100);
    return num;
}
let test = cacheFunc(cb);
//calling invokeCb with first argument
let output1 = test('first');
//calling invokeCb with same argument
let output2 = test('first');
if (output1 === output2) {
    console.log('Code is running');
} else {
    console.log('There is some issue');
}
