const counterFact = require("./counterFactory")
let n = 10
let decValueRes = counterFact.decrement(n)
let decValueCheck = n - 1
n = 10
let incValueRes = counterFact.increment(n)
let incValueCheck = n + 1

test('should check increment', () => {
    expect(incValueCheck).toEqual(incValueRes)
})

test('should check decrement', () => {
    expect(decValueCheck).toEqual(decValueRes)
})